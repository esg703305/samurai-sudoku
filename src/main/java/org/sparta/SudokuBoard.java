package org.sparta;

public class SudokuBoard {
  private static final int BOARD_SIZE = 9;
  private final int[][] board;

  public SudokuBoard(int[][] board) {
    this.board = board;
  }
  public boolean isSolved() {
    if(!isCorrectSize()) {
      return false;
    }
    for(int i = 0; i < BOARD_SIZE; i++) {
      if(duplicatesInRow(i) || duplicatesInColumn(i)) {
        return false;
      }
    }
    int count = 0;
    int x = 0;
    int y = 0;
    while(count < 9) {
      if(x == 9) {
        x = 0;
        y = y + 3;
      }
      if(y == 9) {
        y = 0;
        x = x + 3;
      }
      if(duplicatesInSquare(x, y)) {
        System.out.println("Duplicates in " + count);
        return false;
      };

      x = x +3;
      count++;
    }
    return true;
  }


  private boolean isCorrectSize() {
    return board.length == BOARD_SIZE && board[0].length == BOARD_SIZE;
  }

  private boolean duplicatesInRow(int x) {
    int[] row = new int[BOARD_SIZE];
    for (int i = 0; i < BOARD_SIZE; i++) {
      int value = board[x][i];
      if(value < 1 || value > 9 || row[value-1] != 0) {
        System.out.println("duplicate in row " + x);
        return true;
      }
      row[value-1] = value;
    }
    return false;
  }

  private boolean duplicatesInColumn(int y) {
    int[] column = new int[BOARD_SIZE];
    for (int i = 0; i < BOARD_SIZE; i++) {
      int value = board[i][y];
      if(value < 1 || value > 9 || column[value-1] != 0) {
        System.out.println("duplicate in column " + y);
        return true;
      }
      column[value-1] = value;
    }
    return false;
  }

  private boolean duplicatesInSquare(int x, int y) {
    System.out.printf("Duplicates in Square x:%d, y:%d%n", x, y);
    int[] square = new int[BOARD_SIZE];
    int[] sorted = new int[BOARD_SIZE];
    int count = 0;
    for(int i = x; i < x+3; i++) {
      for(int j = y; j < y+3; j++) {
        square[count] = board[i][j];
        count++;
      }
    }
    for(int i = 0; i < BOARD_SIZE; i++) {
      int value = square[i];
      if(sorted[value-1] != 0) {
        return true;
      }
      sorted[value-1] = value;
    }
    return false;
  }
}